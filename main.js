
//          *
//          0
//         000
//        00000
//       0000000
//      000000000           Code from https://github.com/bob4koolest/fakeCAPTCHA
//     00000000000
//    0000000000000
//   000000000000000
//  00000000000000000

var imgdata = { //add 9 image URLs and a title in the end, you may do up to 6 slides
  firstimgURL: [
    "https://www.motor24.pt/files/2020/12/FER1281.jpg",
    "http://engenhariacivil.files.wordpress.com/2009/09/tgv.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/a/a5/2018_Maserati_Quattroporte_V6_Automatic_3.0_Front.jpg",
    "https://f1-insider.com/wp-content/uploads/2021/04/Formel-1-Max-Verstappen-Imola-FP3-2021.jpg",
    "https://i.pinimg.com/originals/92/d2/1f/92d21f73d0e42b70796fb081c9a981c7.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/e/eb/British_Airways_Concorde_G-BOAC_03.jpg",
    "https://ireland.apollo.olxcdn.com/v1/files/cmnncxdvt36t-PT/image;s=1000x700",
    "https://www.thedrive.com/content/2021/11/f-35-uk.jpg?quality=85",
    "https://1.bp.blogspot.com/-Y9xOuthufIA/YBIUl5ysIdI/AAAAAAAApak/IY3Hn-sFpAoXNkrnTTHblQWeAB1ckQ3SQCLcBGAsYHQ/s2048/Novo-Tesla-Model-S-2022%2B%252829%2529.jpg",
    "the pinnacle of mobility" //title goes last
  ],
  secondimgURL: [
    "https://prettysimplesweet.com/wp-content/uploads/2016/12/ChocolateSalami_01.jpg",
    "https://storcpdkenticomedia.blob.core.windows.net/media/recipemanagementsystem/media/recipe-media-files/recipes/retail/x17/2020_df_retail_chocolate-cake-w-chocolate-buttercream_760x580.jpg?ext=.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Circle_sign_31.svg/1200px-Circle_sign_31.svg.png",
    "https://www.france-export-fv.com/WebRoot/Orange/Shops/6449c484-4b17-11e1-a012-000d609a287c/5392/B8BA/508D/0F9D/C685/0A0C/05E0/67FB/Chocolat.jpg",
    "https://i.pinimg.com/originals/53/a1/a1/53a1a1c32eefe568eeea8b64895acb19.jpg",
    "https://motherwouldknow.com/wp-content/uploads/2017/12/2017-12-19-chocolate-salami-sq-close-up-IMG_8497-w.jpg",
    "https://s2.glbimg.com/8QGipHvweOXdi0rq9cmyNyW0eQw=/0x0:1280x854/924x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/internal_photos/bs/2020/f/z/NC0azsRqaYfFLPbNR1gQ/cheesecake.jpeg",
    "https://wikiimg.tojsiabtv.com/wikipedia/commons/1/18/Salami_aka.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Circle_sign_32.svg/1024px-Circle_sign_32.svg.png",
    "chocolate salami" //title goes last
  ],
  thirdimgURL: [
    "image/image_part_001.png",
    "image/image_part_002.png",
    "image/image_part_003.png",
    "image/image_part_004.png",
    "image/image_part_005.png",
    "image/image_part_006.png",
    "image/image_part_007.png",
    "image/image_part_008.png",
    "image/image_part_009.png",
    "kernel wizards" //title goes last
  ],
  fourthimgURL: [
    "-",
    "-",
    "-",
    "-",
    "-",
    "-",
    "-",
    "-",
    "-",
    "COVID-19" //title goes last
  ],
  /*fifthimgURL: [ //add your new slide here
  
  ] */
};

var correctanswers = { //0 is wrong, 1 is right
  firstimgURL: [
    1,
    0,
    0,
    0,
    1,
    0,
    1,
    0,
    0
  ],
  secondimgURL: [
    1,
    0,
    0,
    0,
    0,
    1,
    0,
    0,
    1
  ],
  thirdimgURL: [
    0,
    1,
    0,
    1,
    1,
    0,
    1,
    1,
    1
  ],
  fourthimgURL: [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
  ],
   /*fifthimgURL: [ //add your answers for slide #5 here
  
  ] */
};

var answerdb = [];

function insertimages(slidenum) {
  var arrayToGet = slidenum + "imgURL";
  window.currentimgset = arrayToGet;
  for(let i = 1; i < 10; i++) {
    var atimage = i.toString();
    document.getElementById(atimage).src = imgdata[arrayToGet][i-1];
  }
  document.getElementById("target-img-name").innerText = imgdata[arrayToGet][9];
}

function resetcaptcha() {
    answerdb = [];
    for(let i = 1; i < 10; i++) {
      if(document.getElementById(i).className === "selected") {
        answerdb.push(1); //selected
      } else {
        answerdb.push(0); //unselected
      }
    }

    if(arrayEquals(answerdb, correctanswers[currentimgset]) == false) { //check if answers are correct
      //incorrect
      for(let i = 1; i < 10; i++) {
        if(document.getElementById(i).className === "selected") {
          document.getElementById(i).className = "unselected";
        }
      }
      document.getElementsByClassName("try-again")[0].style.display = "block";
    } else {
      for(let i = 1; i < 10; i++) {
        if(document.getElementById(i).className === "selected") {
          document.getElementById(i).className = "selected correct";
        }
      }
      document.getElementsByClassName("try-again")[0].style.display = "none";
      setTimeout(function() {nextimg()}, 1500); //for some reason this seems to work
    }
}

function nextimg() {
  for(let i = 1; i < 10; i++) {
    if(document.getElementById(i).className === "selected" || document.getElementById(i).className === "selected correct") {
      document.getElementById(i).className = "unselected";
    }
  }
  initimg()
}

function captchaclick(num) {
  if(document.getElementById(num).className !== "selected" ) {
    document.getElementById(num).className = "selected";
  } else {
    document.getElementById(num).className = "unselected";
  }
}

var place = "none";
function initimg() {
  switch(place) {
    case "none":
      insertimages("first");
      place = "first";
      break;
    case "first":
      insertimages("second");
      place = "second";
      break;
    case "second":
      insertimages("third");
      place = "third";
      break;
    case "third":
      //insertimages("fourth");
      window.location.replace("answer.html");
      place = "fourth";
      break;
    case "fourth":
      //insertimages("fifth");
      place = "fifth";
      window.location.replace("answer.html");
      break;
    // case "fifth":
    //   insertimages("sixth");
    //   place = "sixth";
    //   break;
    default:
      insertimages("first");
      place = "first";
  }

}

function arrayEquals(a, b) {
  return Array.isArray(a) &&
    Array.isArray(b) &&
    a.length === b.length &&
    a.every((val, index) => val === b[index]);
}
